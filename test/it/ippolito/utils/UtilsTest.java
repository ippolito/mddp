package it.ippolito.utils;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import it.ippolito.mddp.exception.InvalidBitPositionException;
import it.ippolito.mddp.utils.MethodUtils;

class UtilsTest {

	@Test
	void test() throws InvalidBitPositionException {
		
		/* 1000 0000 0100 0000 = 8040*/
		
		byte[] byteArray = {
				(byte) /* 4 byte - Length of the message	*/
				0x80,0x40
				};
		
		
		
		assertTrue(MethodUtils.getBit(byteArray, 1));
		assertTrue(MethodUtils.getBit(byteArray, 10));
		assertFalse(MethodUtils.getBit(byteArray, 2));
		assertFalse(MethodUtils.getBit(byteArray, 9));

	}

}
