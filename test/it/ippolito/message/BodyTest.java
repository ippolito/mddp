package it.ippolito.message;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.ippolito.mddp.contract.Contract;
import it.ippolito.mddp.core.BodyDecoder;
import it.ippolito.mddp.core.BodyEncoder;
import it.ippolito.mddp.domain.Body;
import it.ippolito.mddp.exception.InvalidBitPositionException;
import it.ippolito.mddp.exception.InvalidBodyException;
import it.ippolito.mddp.exception.InvalidFieldLengthException;
import it.ippolito.mddp.exception.InvalidHeaderException;
import it.ippolito.mddp.utils.MethodUtils;

class BodyTest {

	@Test
	void test() throws InvalidHeaderException, UnsupportedEncodingException, InvalidBodyException, InvalidBitPositionException {
		
		byte[] byteArray = {
				/* 1 byte - bitmap length*/
				0x01,				
				(byte) /* 1 byte - bitmap	*/
				0x80,
				/* 1 Field - keepAlive - variableString */
				0x00,0x03,0x41,0x42,0x43
				};
		
	    ObjectMapper mapper = new ObjectMapper();
	    Contract sample = null;
		  try {
			sample = mapper.readValue(Paths.get("resources/contract.json").toFile(), Contract.class);
			sample.print();
		} catch (StreamReadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatabindException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		Body body = BodyDecoder.decode(byteArray,sample.getFunctions().get(0).getVersions().get(0),false,sample.getCharset());
		System.out.println(body.getFields().get("keepAlive"));
		assertTrue(true);
	
	}

	@Test
	void test2() throws InvalidHeaderException, UnsupportedEncodingException, InvalidBodyException, InvalidBitPositionException {
		
		byte[] byteArray = {
				/* 1 byte - bitmap length*/
				0x01,				
				(byte) /* 1 byte - bitmap	*/
				0x40,
				/* 2 Field - name - variableString */
				0x00,0x03,0x41,0x42,0x43
				};
		
	    ObjectMapper mapper = new ObjectMapper();
	    Contract sample = null;
		  try {
			sample = mapper.readValue(Paths.get("resources/contract.json").toFile(), Contract.class);
			sample.print();
		} catch (StreamReadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatabindException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		Body body = BodyDecoder.decode(byteArray,sample.getFunctions().get(0).getVersions().get(0),false,sample.getCharset());
		System.out.println(body.getFields().get("name"));
		assertTrue(true);
	
	}

	@Test
	void test3() throws InvalidHeaderException, UnsupportedEncodingException, InvalidBodyException, InvalidBitPositionException {
		
		byte[] byteArray = {
				/* 1 byte - bitmap length*/
				0x01,				
				(byte) /* 1 byte - bitmap	*/
				0x60,
				/* 2 Field - name - variableString */
				0x00,0x03,0x41,0x42,0x43,
				/* 3 Field - description - variableString */
				0x00,0x03,0x44,0x45,0x46
			};
		
	    ObjectMapper mapper = new ObjectMapper();
	    Contract sample = null;
		  try {
			sample = mapper.readValue(Paths.get("resources/contract.json").toFile(), Contract.class);
			sample.print();
		} catch (StreamReadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatabindException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		Body body = BodyDecoder.decode(byteArray,sample.getFunctions().get(0).getVersions().get(0),false,sample.getCharset());
		System.out.println(body.getFields().get("name"));
		System.out.println(body.getFields().get("description"));
		assertTrue(true);
	
	}
	
	
	

	@SuppressWarnings("unchecked")
	@Test
	void testComplex() throws InvalidHeaderException, UnsupportedEncodingException, InvalidBodyException, InvalidBitPositionException {
		
		byte[] byteArray = {
				/* 1 byte - bitmap length*/
				0x01,				
				(byte) /* 1 byte - bitmap	*/
				0x50      ,
				/* 2 Field - name - variableString */
				0x00,0x03,0x41,0x42,0x43,
				/* 4 Field - complexData - Complex */
				/* lunghezza del dato complesso*/
						0x00,0x0A,
				/*  complex - lunghezza bitmap */				
						0x01,				
				/*  complex -  bitmap */				
						(byte) 0xC0,		
				/*	complex - subfield 1 */
						0x44,0x45,0x46,						
				/*	complex - subfield 2*/
						0x00,0x03,0x41,0x42,0x43,

			};
		
	    ObjectMapper mapper = new ObjectMapper();
	    Contract sample = null;
		  try {
			sample = mapper.readValue(Paths.get("resources/contract.json").toFile(), Contract.class);
			sample.print();
		} catch (StreamReadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatabindException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		Body body = BodyDecoder.decode(byteArray,sample.getFunctions().get(0).getVersions().get(0),false,sample.getCharset());
		System.out.println(body.getFields().get("name"));
		System.out.println(((Map<String,Object>) body.getFields().get("complexData")).get("field2"));
		assertTrue(true);
	
	}


	@Test
	void test4() throws InvalidHeaderException, UnsupportedEncodingException, InvalidBodyException, InvalidBitPositionException, InvalidFieldLengthException {
		

		
	    ObjectMapper mapper = new ObjectMapper();
	    Contract sample = null;
		  try {
			sample = mapper.readValue(Paths.get("resources/contract.json").toFile(), Contract.class);
			sample.print();
		} catch (StreamReadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatabindException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		  Map<String,Object> fields = new HashMap<String,Object>();
		  fields.put("responseCode", "000");

		  byte[] array = BodyEncoder.encode(fields, sample.getFunctions().get(0).getVersions().get(0), true, sample.getCharset());
		  
		  System.out.println(MethodUtils.bytesToHex(array));
		  
		  assertTrue(array.length == 5);
	
	}
	

	@Test
	void test5() throws InvalidHeaderException, UnsupportedEncodingException, InvalidBodyException, InvalidBitPositionException, InvalidFieldLengthException {
		

		
	    ObjectMapper mapper = new ObjectMapper();
	    Contract sample = null;
		  try {
			sample = mapper.readValue(Paths.get("resources/contract.json").toFile(), Contract.class);
			sample.print();
		} catch (StreamReadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatabindException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		  Map<String,Object> fields = new HashMap<String,Object>();
		  fields.put("responseCode", "000");
		  fields.put("description", "OK");

		  byte[] array = BodyEncoder.encode(fields, sample.getFunctions().get(0).getVersions().get(0), true, sample.getCharset());
		  
		  System.out.println(MethodUtils.bytesToHex(array));
		  
		  assertTrue(array.length == 5 + ((String)fields.get("description")).length() + 2);
	
	}


	@Test
	void testComplexEncode() throws InvalidHeaderException, UnsupportedEncodingException, InvalidBodyException, InvalidBitPositionException, InvalidFieldLengthException {
		

		
	    ObjectMapper mapper = new ObjectMapper();
	    Contract sample = null;
		  try {
			sample = mapper.readValue(Paths.get("resources/contract.json").toFile(), Contract.class);
			sample.print();
		} catch (StreamReadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatabindException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		  Map<String,Object> fieldsComplex = new HashMap<String,Object>();
		  fieldsComplex.put("field2", "prova");
		  
		  Map<String,Object> fields = new HashMap<String,Object>();
		  fields.put("description", "OK");
		  fields.put("complexData",fieldsComplex);

		  
		  
		  byte[] array = BodyEncoder.encode(fields, sample.getFunctions().get(0).getVersions().get(0), false, sample.getCharset());
		  
		  System.out.println(MethodUtils.bytesToHex(array));
		  
		  assertTrue(true);

	}

}
