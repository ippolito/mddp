package it.ippolito.message;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import it.ippolito.mddp.core.HeaderDecoder;
import it.ippolito.mddp.domain.Header;
import it.ippolito.mddp.exception.InvalidHeaderException;

class HeaderTest {

	@Test
	void test() throws InvalidHeaderException {
		
		byte[] byteArray = {
				/* 4 byte - Length of the message	*/
				0x00,0x00,0x00,0x0C,
				/* 1 byte - Number of the protocol Version */
				0x01,
				/* 1 byte - Field Status flag */
				0x00,
				/*	 * 2 byte - Contract Version  (numerico) */
				0x00,0x01,
				/* 2 byte - Function Number	(numerico)	*/ 
				0x00,0x01,
				 /* 2 byte - Function Version  (numerico) */
				0x00,0x01,		
				/* 4 byte - Correlation Key	*/
				0x00,0x00,0x2F,0x0E
				};
		
		Header header = HeaderDecoder.decode(byteArray);
		header.printHeader();
		
		assertTrue(true);
	
	}

}
