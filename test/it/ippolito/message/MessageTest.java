package it.ippolito.message;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Paths;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.ippolito.mddp.contract.Contract;
import it.ippolito.mddp.core.MessageEncoder;
import it.ippolito.mddp.domain.Header;
import it.ippolito.mddp.exception.InvalidBitPositionException;
import it.ippolito.mddp.exception.InvalidBodyException;
import it.ippolito.mddp.exception.InvalidFieldLengthException;
import it.ippolito.mddp.exception.InvalidHeaderException;
import it.ippolito.mddp.utils.MethodUtils;

class MessageTest {



	@Test
	void test() throws InvalidHeaderException, UnsupportedEncodingException, InvalidBodyException, InvalidBitPositionException, InvalidFieldLengthException {
		

		
	    ObjectMapper mapper = new ObjectMapper();
	    Contract sample = null;
		  try {
			sample = mapper.readValue(Paths.get("resources/contract.json").toFile(), Contract.class);
			sample.print();
		} catch (StreamReadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatabindException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		  Map<String,Object> fields = new HashMap<String,Object>();
		  fields.put("responseCode", "000");
		  fields.put("description", "OK");

		  Header header = new Header();
		  header.setContractVersion(1);
		  header.setCorrelationKey(1);
		  header.setFunctionNumber(0);
		  header.setFunctionVersion(0);
		  header.setProtocolVersion((short)1);
		  header.setStatusFlag(BitSet.valueOf(new byte[]{0x01}));
		  
		  byte[] array = MessageEncoder.encode(header, sample, fields);
		  
		  System.out.println(MethodUtils.bytesToHex(array));
		  System.out.println("Msg Length:" + array.length);

		  
		  assertTrue(true);
	
	}
	

	@Test
	void test2() throws InvalidHeaderException, UnsupportedEncodingException, InvalidBodyException, InvalidBitPositionException, InvalidFieldLengthException {
		

		
	    ObjectMapper mapper = new ObjectMapper();
	    Contract sample = null;
		  try {
			sample = mapper.readValue(Paths.get("resources/contract.json").toFile(), Contract.class);
			sample.print();
		} catch (StreamReadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatabindException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		  Map<String,Object> fields = new HashMap<String,Object>();
		  fields.put("keepAlive", "ECHO");
		  fields.put("name", "test");
		  fields.put("description", "MDDP TEST");

		  Header header = new Header();
		  header.setContractVersion(1);
		  header.setCorrelationKey(1);
		  header.setFunctionNumber(0);
		  header.setFunctionVersion(0);
		  header.setProtocolVersion((short)1);
		  header.setStatusFlag(BitSet.valueOf(new byte[]{0x00}));
		  
		  byte[] array = MessageEncoder.encode(header, sample, fields);
		  
		  System.out.println(MethodUtils.bytesToHex(array));
		  System.out.println("Msg Length:" + array.length);
		  
		  String jsonEquivalent = "{\"keepAlive\": \"ECHO”,\"name”: \"test”,“description”: \"MDDP TEST”}";
		  System.out.println("Json Equivalent Length:" + jsonEquivalent.length());

		  assertTrue(true);
	
	}
}
