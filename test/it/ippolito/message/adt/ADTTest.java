package it.ippolito.message.adt;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import it.ippolito.mddp.contract.Field;
import it.ippolito.mddp.contract.enumeration.DataType;
import it.ippolito.mddp.core.BodyDecoder;
import it.ippolito.mddp.exception.InvalidBitPositionException;
import it.ippolito.mddp.exception.InvalidBodyException;
import it.ippolito.mddp.exception.InvalidHeaderException;

@SuppressWarnings("unchecked")
class ADTTest extends BodyDecoder {

	@Test
	void listDecode() throws InvalidHeaderException, UnsupportedEncodingException, InvalidBodyException, InvalidBitPositionException {
		
		byte[] byteArray = {
				/* 2 byte - field length*/
//				0x00,0x0B,			
				/* element 1 -  variableString */
				0x00,0x03,0x41,0x42,0x43,
				/* element 2 -  variableString */
				0x00,0x05,0x44,0x45,0x46,0x47,0x48
				};
		Field stringField = new Field();
		stringField.setDataType(DataType.String);
		stringField.setLength(-1);
		stringField.setDescription("");
		stringField.setName("stringElement");
		
		
		
		
		Field listField = new Field();
		listField.setDataType(DataType.List);
		listField.setLength(-1);
		listField.setDescription("");
		listField.setName("testList");
		listField.setSubfields(new ArrayList<Field>());
		listField.getSubfields().add(stringField);
		

		List<String> value = (List<String>) BodyDecoder.createList(byteArray, stringField, "UTF-8");
		System.out.println("*** LISTA ***");			

		for(String v: value) {
			System.out.println(v);			
		}
		assertTrue(true);
	
	}
	
	@Test
	void listDecode2() throws InvalidHeaderException, UnsupportedEncodingException, InvalidBodyException, InvalidBitPositionException {
		
		byte[] byteArray = {
				/* 2 byte - field length*/
//				0x00,0x0B,			
				/* element 1 -  FIXED STRING */
				0x41,0x42,0x43,
				/* element 2 -  FIXED STRING */
				0x44,0x45,0x46,
				/* element 3 -  FIXED STRING */
				0x47,0x48,0X49
				};
		Field stringField = new Field();
		stringField.setDataType(DataType.String);
		stringField.setLength(3);
		stringField.setDescription("");
		stringField.setName("stringElement");
		
		

		List<String> value = (List<String>) BodyDecoder.createList(byteArray, stringField, "UTF-8");
		System.out.println("*** LISTA ***");			

		for(String v: value) {
			System.out.println(v);			
		}
		assertTrue(true);
	
	}

	@Test
	void listDecode3() throws InvalidHeaderException, UnsupportedEncodingException, InvalidBodyException, InvalidBitPositionException {
		
		byte[] byteArray = {
				/* 2 byte - field length*/
//				0x00,0x0B,			
				0x00,0x0B,
				/* element 1 -  variable STRING */
				0x00,0x03,0x41,0x42,0x43,
				/* element 2 -  variable STRING */
				0x00,0x04,0x44,0x45,0x46,0x47,
				};
		Field stringField = new Field();
		stringField.setDataType(DataType.String);
		stringField.setLength(-1);
		stringField.setDescription("");
		stringField.setName("stringElement");
		
		Field listField = new Field();
		listField.setDataType(DataType.List);
		listField.setLength(-1);
		listField.setDescription("");
		listField.setName("testList");
		listField.setSubfields(new ArrayList<Field>());
		listField.getSubfields().add(stringField);

		List<List<String>> value = (List<List<String>>) BodyDecoder.createList(byteArray, listField, "UTF-8");
		System.out.println("*** LISTA ***");			

		for(String v: value.get(0)) {
			System.out.println(v);			
		}
		assertTrue(true);
	
	}
	
	

	@Test
	void mapDecode() throws InvalidHeaderException, UnsupportedEncodingException, InvalidBodyException, InvalidBitPositionException {
		
		byte[] byteArray = {
				/* 2 byte - field length*/
//				0x00,0x0B,			
				/* element 1 -  FIXED STRING */
				0x41,0x42,0x43,
				/* element 2 -  FIXED STRING */
				0x44,0x45,0x46,
				};
		Field keyField = new Field();
		keyField.setDataType(DataType.String);
		keyField.setLength(3);
		keyField.setDescription("");
		keyField.setName("stringKey");
		
		Field valueField = new Field();
		valueField.setDataType(DataType.String);
		valueField.setLength(3);
		valueField.setDescription("");
		valueField.setName("stringValue");
				

		Map<String,String> value = (Map<String,String>) BodyDecoder.createMap(byteArray, keyField,valueField, "UTF-8");
		System.out.println("*** DICTIONARY ***");			

			System.out.println(value.get("ABC"));			

		assertTrue(true);
	
	}
	
	
}
