package it.ippolito.mddp.utils;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Arrays;

import it.ippolito.mddp.exception.InvalidBitPositionException;

public class MethodUtils {
	private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

	public static final long getLong(byte[] bytes) {
		
		ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
		byte [] b = {0x00,0x00,0x00,0x00, bytes[0],bytes[1],bytes[2],bytes[3]};
		buffer.put(b);
		buffer.rewind();
		return buffer.getLong();

	}

	public static final byte[] getByteFromLong(long i) {
		ByteBuffer buffer = ByteBuffer.allocate(8);
		buffer.putLong(i);
		return Arrays.copyOfRange(buffer.array(), 4, 8);
	}
	
	
	public static final int getInteger(byte[] bytes) {
		
		ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
		byte [] b = {0x00,0x00, bytes[0],bytes[1]};
		buffer.put(b);
		buffer.rewind();
		return buffer.getInt();

	}

	public static final byte[] getByteFromInteger(int i) {
		ByteBuffer buffer = ByteBuffer.allocate(4);
		buffer.putInt(i);
		return Arrays.copyOfRange(buffer.array(), 2, 4);
	}
	
	public static final short getShort(byte[] bytes) {
		ByteBuffer buffer = ByteBuffer.allocate(Short.BYTES);
		byte [] b = {0x00, bytes[0]};
		buffer.put(b);
		buffer.rewind();
		return (short) (buffer.getShort());

	}
	
	public static final byte[] getByteFromShort(short i) {
		ByteBuffer buffer = ByteBuffer.allocate(2);
		buffer.putShort(i);
		return Arrays.copyOfRange(buffer.array(), 1, 2);
	}	
	
	public static final byte getByteFromBoolean(boolean b) {
		if(b) {
			return 0x01;
		}else {
			return 0x00;
		}
	}
	
	public static final boolean getBit(byte[] array, int position) throws InvalidBitPositionException{
		if(position >= array.length*8 || position < 0) {
			throw new InvalidBitPositionException();
		}
		
		int byteNum = position/8;
		int bitPos = (position % 8);
		return BigInteger.valueOf(array[byteNum]).testBit(7 - bitPos);
		
	}
	
	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for (int j = 0; j < bytes.length; j++) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = HEX_ARRAY[v >>> 4];
	        hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
	    }
	    return new String(hexChars);
	}

}
