package it.ippolito.mddp.core;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import it.ippolito.mddp.contract.Contract;
import it.ippolito.mddp.domain.Body;
import it.ippolito.mddp.domain.Header;
import it.ippolito.mddp.domain.Message;
import it.ippolito.mddp.exception.BodyFormatErrorException;
import it.ippolito.mddp.exception.HeaderErroFlagRaisedException;
import it.ippolito.mddp.exception.InvalidBitPositionException;
import it.ippolito.mddp.exception.InvalidBodyException;
import it.ippolito.mddp.exception.InvalidHeaderException;

public class MessageDecoder {

	public static final Message decode(byte[] data, Contract contract) throws InvalidHeaderException, HeaderErroFlagRaisedException, BodyFormatErrorException   {
		Header header = HeaderDecoder.decode(data);
		
		if(header.getResponseStatusFlag(contract).nextSetBit(3) != -1) {
			throw new HeaderErroFlagRaisedException();
		}
		
		byte[] bodyData = Arrays.copyOfRange(data, header.getHeaderLength(), data.length);

		Body body = null;
			
		 try {
			body = BodyDecoder.decode(bodyData,
					contract.getFunctions().get(header.getFunctionNumber()).getVersions().get(header.getFunctionVersion()),
					header.isResponse(), contract.getCharset());
		} catch (UnsupportedEncodingException | InvalidBitPositionException | InvalidBodyException e) {
			//TODO settare format error e messaggio non corretto
			throw new BodyFormatErrorException();
		}

		return new Message(header, body, contract);

	}


}
