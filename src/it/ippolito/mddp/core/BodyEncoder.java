package it.ippolito.mddp.core;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;

import it.ippolito.mddp.contract.Field;
import it.ippolito.mddp.contract.FunctionVersion;
import it.ippolito.mddp.exception.InvalidBitPositionException;
import it.ippolito.mddp.exception.InvalidBodyException;
import it.ippolito.mddp.exception.InvalidFieldLengthException;
import it.ippolito.mddp.utils.MethodUtils;

public class BodyEncoder {

	public static byte[] encode(Map<String, Object> fields, FunctionVersion fv, boolean isResponse, String charset)
			throws InvalidBodyException, UnsupportedEncodingException, InvalidBitPositionException,
			InvalidFieldLengthException {

		List<Field> fieldList;
		if (isResponse) {
			fieldList = fv.getResponse();
		} else {
			fieldList = fv.getRequest();
		}

		return parseComplex(fields, fieldList, charset);

	}

	private static byte[] parseComplex(Map<String, Object> fields, List<Field> fieldList, String charset)
			throws UnsupportedEncodingException, InvalidFieldLengthException {
		ByteBuffer bb = ByteBuffer.allocate(0);
		String bitmapString = "";
		for (int i = 0; i < fieldList.size(); i++) {
			Object obj = fields.get(fieldList.get(i).getName());
			if (obj != null) {
				byte[] newFieldArray = getArrayValue(obj, fieldList.get(i), charset);
				byte[] oldFieldsArray = bb.array();
				bb = ByteBuffer.allocate(newFieldArray.length + oldFieldsArray.length);
				bb.put(oldFieldsArray);
				bb.put(newFieldArray);

				bitmapString += "1";
			} else {
				bitmapString += "0";
			}
		}
		int k = bitmapString.length() % 8;
		while (k != 0) {
			bitmapString = bitmapString.concat("0");
			k = bitmapString.length() % 8;
		}
		byte[] fieldsBA = bb.array();
		bb = ByteBuffer.allocate(1 + (bitmapString.length() / 8) + fieldsBA.length);
		bb.put(MethodUtils.getByteFromShort(Integer.valueOf((bitmapString.length() / 8)).shortValue()));
		for (int i = 0; i < (bitmapString.length() / 8); i++) {
			bb.put(MethodUtils.getByteFromShort(Short.parseShort(bitmapString.substring(i * 8, (i + 1) * 8), 2)));
		}

		bb.put(fieldsBA);

		return bb.array();
	}

	@SuppressWarnings("unchecked")
	public static byte[] getArrayValue(Object obj, Field field, String charset)
			throws UnsupportedEncodingException, InvalidFieldLengthException {

		ByteBuffer bb = null;
		switch (field.getDataType()) {
		case Binary:
			int objLen = ((byte[]) obj).length;
			if (objLen != field.getLength() && field.getLength() != -1) {
				throw new InvalidFieldLengthException();
			}
			return getArrayBuffer(((byte[]) obj), field);
		case List:
			return getArrayBuffer(getListArrayBuffer(obj, field.getSubfields().get(0), charset), field);
		case Dictionary:
			return getArrayBuffer(
					getMapArrayBuffer(obj, field.getSubfields().get(0), field.getSubfields().get(1), charset), field);
		case Complex:
			Map<String, Object> fields = (Map<String, Object>) obj;
			return getArrayBuffer(parseComplex(fields, field.getSubfields(), charset), field);
		case String:
			String s = new String(((String) obj).getBytes(), charset);
			if (s.getBytes().length != field.getLength() && field.getLength() != -1) {
				throw new InvalidFieldLengthException();
			}
			return getArrayBuffer(s.getBytes(), field);
		case Boolean:
			bb = ByteBuffer.allocate(1);
			bb.put(MethodUtils.getByteFromBoolean((boolean) obj));
			return bb.array();
		case Integer:
			bb = ByteBuffer.allocate(Integer.BYTES);
			bb.putInt((int) obj);
			return bb.array();
		case Long:
			bb = ByteBuffer.allocate(Long.BYTES);
			bb.putLong((long) obj);
			return bb.array();
		case Double:
			bb = ByteBuffer.allocate(Double.BYTES);
			bb.putDouble((double) obj);
			return bb.array();
		case Float:
			bb = ByteBuffer.allocate(Float.BYTES);
			bb.putFloat((float) obj);
			return bb.array();
		default:
			byte[] array = {};
			return array;
		}

	}

	@SuppressWarnings("unchecked")
	private static byte[] getMapArrayBuffer(Object obj, Field fieldKey, Field fieldValue, String charset)
			throws UnsupportedEncodingException, InvalidFieldLengthException {

		byte[] array = null;
		Map<Object, Object> map = (Map<Object, Object>) obj;
		for (Map.Entry<Object, Object> set : map.entrySet()) {
			byte[] newValueKey = getArrayValue(set.getKey(), fieldKey, charset);
			byte[] newValueValue = getArrayValue(set.getValue(), fieldValue, charset);

			ByteBuffer bb = ByteBuffer.allocate(array.length + newValueKey.length + newValueValue.length);
			bb.put(array);
			bb.put(newValueKey);
			bb.put(newValueValue);
			array = bb.array();
		}
		return array;
	}

	@SuppressWarnings("unchecked")
	private static byte[] getListArrayBuffer(Object obj, Field fieldElement, String charset)
			throws UnsupportedEncodingException, InvalidFieldLengthException {
		// TODO Auto-generated method stub

		byte[] array = null;
		List<Object> list = (List<Object>) obj;
		for (Object o : list) {
			byte[] newValue = getArrayValue(o, fieldElement, charset);

			ByteBuffer bb = ByteBuffer.allocate(array.length + newValue.length);
			bb.put(array);
			bb.put(newValue);
			array = bb.array();
		}
		return array;
	}

	private static byte[] getArrayBuffer(byte[] obj, Field field) {
		ByteBuffer bb;
		int objLen = ((byte[]) obj).length;
		if (field.getLength() == -1) {
			bb = ByteBuffer.allocate(objLen + 2);
			bb.put(MethodUtils.getByteFromInteger(objLen));
		} else {
			bb = ByteBuffer.allocate(objLen);
		}
		bb.put(obj);
		return bb.array();
	}

}
