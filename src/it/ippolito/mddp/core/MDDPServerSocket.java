package it.ippolito.mddp.core;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketImpl;

public class MDDPServerSocket extends ServerSocket{

	
	public MDDPServerSocket(int port) throws IOException{
		super(port);
	}
	@Override
	public Socket accept() throws IOException {
	    if (isClosed())
	        throw new SocketException("Socket is closed");
	    if (!isBound())
	        throw new SocketException("Socket is not bound yet");
	    Socket s = new MDDPSocket((SocketImpl) null);
	    implAccept(s);
	    return s;
	}
	
	public MDDPSocket getConnection() throws IOException {
		return (MDDPSocket)(this.accept());
	}
}
