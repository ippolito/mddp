package it.ippolito.mddp.core;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.ippolito.mddp.contract.Contract;
import it.ippolito.mddp.exception.InvalidContractException;

public class ContractsHandler {

	private static final String DEFAULT_CONTRACT_PATH = "resources/contract.json";
	private static final String DEFAULT_CONTRACT_KEY = "default";
	private static  ContractsHandler instance;
	private static Map<String,Contract> contracts;
	
	public static ContractsHandler instance() throws InvalidContractException{
		if(instance == null) {
			instance = new ContractsHandler();
		}
		return instance;
	}
	
	private ContractsHandler() throws InvalidContractException {
		contracts = new HashMap<String, Contract>();
		contracts.put(DEFAULT_CONTRACT_KEY, getContract(DEFAULT_CONTRACT_PATH));
	}
	
	
	
	private static Contract getContract(String path) throws InvalidContractException {
	    ObjectMapper mapper = new ObjectMapper();
		   
		  try {
			return mapper.readValue(Paths.get(path).toFile(), Contract.class);
		} catch (StreamReadException e) {
			throw new InvalidContractException(e);
		} catch (DatabindException e) {
			// TODO Auto-generated catch block
			throw new InvalidContractException(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new InvalidContractException(e);
		}
	}
	
	
	public  Contract getDefaultContract() {
		return contracts.get(DEFAULT_CONTRACT_KEY);
	}
	
}
