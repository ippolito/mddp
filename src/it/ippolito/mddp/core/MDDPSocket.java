package it.ippolito.mddp.core;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketImpl;
import java.nio.ByteBuffer;

import it.ippolito.mddp.domain.Message;
import it.ippolito.mddp.exception.BodyFormatErrorException;
import it.ippolito.mddp.exception.HeaderErroFlagRaisedException;
import it.ippolito.mddp.exception.InvalidBitPositionException;
import it.ippolito.mddp.exception.InvalidBodyException;
import it.ippolito.mddp.exception.InvalidContractException;
import it.ippolito.mddp.exception.InvalidFieldLengthException;
import it.ippolito.mddp.exception.InvalidHeaderException;
import it.ippolito.mddp.utils.MethodUtils;

public class MDDPSocket extends Socket{

	
	public  MDDPSocket(InetAddress ipAddr, int port) throws IOException {
		super(ipAddr,port);
	}
	
	public  MDDPSocket(Socket socket) throws IOException {		
		super(socket.getInetAddress(),socket.getPort(),socket.getLocalAddress(),socket.getLocalPort());
	}
	
	public MDDPSocket(SocketImpl socketImpl) throws SocketException {
		super(socketImpl);
	}

	public void sendMessage(Message message) throws InvalidHeaderException, InvalidBodyException, InvalidBitPositionException, InvalidFieldLengthException, IOException {
		byte[] array = MessageEncoder.encode(message);
		
		OutputStream os = this.getOutputStream();
		os.write(array);
	}
	
	public Message receiveMessage() throws IOException, InvalidHeaderException, InvalidBitPositionException, InvalidBodyException, InvalidContractException, HeaderErroFlagRaisedException, BodyFormatErrorException{
		
		InputStream is = this.getInputStream();
		byte[] headerLengthBuffer = new byte[4];
		
		is.read(headerLengthBuffer);
		
		int headerLength = Long.valueOf(MethodUtils.getLong(headerLengthBuffer)).intValue(); 
		 
		byte[] messagePayload = new byte[headerLength - 4];
		is.read(messagePayload);
		ByteBuffer bb = ByteBuffer.allocate(headerLength);
		bb.put(headerLengthBuffer);
		bb.put(messagePayload);
		return MessageDecoder.decode(bb.array(), ContractsHandler.instance().getDefaultContract());
		 		
	}
	
}
