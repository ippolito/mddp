package it.ippolito.mddp.core;

import java.util.Arrays;
import java.util.BitSet;

import it.ippolito.mddp.domain.Header;
import it.ippolito.mddp.exception.InvalidHeaderException;
import it.ippolito.mddp.utils.MethodUtils;

public class HeaderDecoder {

	public static final Header decode(byte[] header) throws InvalidHeaderException {

		if (header.length < 16) {
			throw new InvalidHeaderException();
		}

		int from = 0;
		int to = 4;

		long messageLength = MethodUtils.getLong(Arrays.copyOfRange(header, from, from + to));
		from += to;
		to = 1;
		short protocolVersion = MethodUtils.getShort(Arrays.copyOfRange(header, from, from + to));
		from += to;
		to = 1;

		if (protocolVersion != 1) {
			throw new InvalidHeaderException();
		}

		// setHeaderLength(16);

		BitSet statusFlag = BitSet.valueOf(Arrays.copyOfRange(header, from, from + to + 1));
		from += to;
		to = 2;
		int contractVersion = MethodUtils.getInteger(Arrays.copyOfRange(header, from, from + to));
		from += to;
		to = 2;
		int functionNumber = MethodUtils.getInteger(Arrays.copyOfRange(header, from, from + to));
		from += to;
		to = 2;
		int functionVersion = MethodUtils.getInteger(Arrays.copyOfRange(header, from, from + to));
		from += to;
		to = 4;
		long correlationKey = MethodUtils.getLong(Arrays.copyOfRange(header, from, from + to));

		return new Header(messageLength, protocolVersion, statusFlag, contractVersion, functionNumber, functionVersion,
				correlationKey, 16);

	}


}
