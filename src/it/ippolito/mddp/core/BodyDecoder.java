package it.ippolito.mddp.core;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.ippolito.mddp.contract.Field;
import it.ippolito.mddp.contract.FunctionVersion;
import it.ippolito.mddp.domain.Body;
import it.ippolito.mddp.exception.InvalidBitPositionException;
import it.ippolito.mddp.exception.InvalidBodyException;
import it.ippolito.mddp.utils.MethodUtils;

public class BodyDecoder {

	public static final Body decode(byte[] body, FunctionVersion fv, boolean isResponse, String charset)
			throws UnsupportedEncodingException, InvalidBitPositionException, InvalidBodyException {

		if (body.length < 1) {
			throw new InvalidBodyException("Bitmap Length not present");
		}
		List<Field> fields;

		if (isResponse) {
			fields = fv.getResponse();
		} else {
			fields = fv.getRequest();
		}

		Map<String, Object> fieldsObject = parseComplex(body, fields, charset);

		return new Body(fieldsObject, charset);

	}

	private static final Map<String, Object> parseComplex(byte[] array, List<Field> fields, String charset)
			throws UnsupportedEncodingException, InvalidBitPositionException, InvalidBodyException {
		int from = 0;
		int to = 1;
		// get bitmap length

		int bitmapLength = MethodUtils.getShort(Arrays.copyOfRange(array, from, from + to));

		if (array.length - 1 < bitmapLength) {
			throw new InvalidBodyException("Invalid Bitmap Length");
		}
		// no bitmap, for example test message
		if (bitmapLength == 0) {
			return null;
		}

		// Loading bitmap
		from += to;
		to = bitmapLength;

		byte[] bitmap = Arrays.copyOfRange(array, from, from + to);
		// loading fields

		from += to;
		to = array.length - 1;

		// load array field values

		byte[] fieldsByteArray = Arrays.copyOfRange(array, from, from + to + 1);

		return extractsFields(fieldsByteArray, fields, bitmap, charset);

	}

	private static final Map<String, Object> extractsFields(byte[] fieldsByteArray, List<Field> fields, byte[] bitmap,
			String charset) throws UnsupportedEncodingException, InvalidBitPositionException, InvalidBodyException {
		Map<String, Object> fieldsObject = new HashMap<String, Object>();

		int k = 0;

		for (int i = 0; i < bitmap.length * 8; ++i) {
			if (MethodUtils.getBit(bitmap, i)) {
				if (i == fields.size()) {
					break;
				}
				k = extractFieldValue(fieldsByteArray, i, fields.get(i), fieldsObject, charset);
				fieldsByteArray = Arrays.copyOfRange(fieldsByteArray, k, fieldsByteArray.length);
			}
		}

		return fieldsObject;

	}

	/* return the bytes used to extract the Field Value */
	private static final int extractFieldValue(byte[] array, int i, Field field, Map<String, Object> fieldsObj,
			String charset) throws UnsupportedEncodingException, InvalidBitPositionException, InvalidBodyException {
		int varByteLenUsed = 0;
		int fieldLength;

		fieldLength = getFieldLength(array, field);
		if (field.getLength() == -1) {
			array = Arrays.copyOfRange(array, 2, array.length);
			varByteLenUsed += 2;
		}

		byte[] fieldArray = Arrays.copyOfRange(array, 0, fieldLength);
		array = Arrays.copyOfRange(array, fieldLength, array.length);

		fieldsObj.put(field.getName(), getObject(fieldArray, field, fieldLength, charset));

		return varByteLenUsed + fieldLength;
	}

	protected static Object createMap(byte[] array, Field fieldKey, Field fieldValue, String charset)
			throws UnsupportedEncodingException, InvalidBitPositionException, InvalidBodyException {
		Map<Object, Object> map = new HashMap<Object, Object>();
		/* The array is */
		while (array.length > 0) {
			int fieldLength;

			/* Field Key */

			fieldLength = getFieldLength(array, fieldKey);
			if (fieldKey.getLength() == -1) {
				array = Arrays.copyOfRange(array, 2, array.length);
			}

			byte[] fieldArray = Arrays.copyOfRange(array, 0, fieldLength);
			array = Arrays.copyOfRange(array, fieldLength, array.length);

			Object key = getObject(fieldArray, fieldKey, fieldLength, charset);

			/* Field Value */

			fieldLength = getFieldLength(array, fieldKey);
			if (fieldKey.getLength() == -1) {
				array = Arrays.copyOfRange(array, 2, array.length);
			}

			fieldArray = Arrays.copyOfRange(array, 0, fieldLength);
			array = Arrays.copyOfRange(array, fieldLength, array.length);

			Object value = getObject(fieldArray, fieldKey, fieldLength, charset);
			map.put(key, value);

		}
		return map;
	}

	protected static Object createList(byte[] array, Field fieldElement, String charset)
			throws UnsupportedEncodingException, InvalidBitPositionException, InvalidBodyException {
		List<Object> list = new ArrayList<Object>();
		/* The array is */
		while (array.length > 0) {
			int fieldLength;

			fieldLength = getFieldLength(array, fieldElement);
			if (fieldElement.getLength() == -1) {
				array = Arrays.copyOfRange(array, 2, array.length);
			}

			byte[] fieldArray = Arrays.copyOfRange(array, 0, fieldLength);
			array = Arrays.copyOfRange(array, fieldLength, array.length);

			list.add(getObject(fieldArray, fieldElement, fieldLength, charset));

		}
		return list;
	}

	private final static int getFieldLength(byte[] array, Field field) {
		int fieldLength;
		// only for not numeric field i'm going to check the length
		switch (field.getDataType()) {
		case Binary:
		case String:
		case List:
		case Dictionary:
		case Complex:
			if (field.getLength() == -1) {
				fieldLength = MethodUtils.getInteger(Arrays.copyOfRange(array, 0, 2));
//					array = Arrays.copyOfRange(array, 2, array.length);
//					varByteLenUsed += 2;
			} else {
				fieldLength = field.getLength();
			}
			break;
		case Boolean:
			fieldLength = 1;
			break;
		case Integer:
			fieldLength = 4;
			break;
		case Long:
			fieldLength = 8;
			break;
		case Double:
			fieldLength = 8;
			break;
		case Float:
			fieldLength = 4;
			break;
		default:
			fieldLength = 0;
			break;
		}
		return fieldLength;
	}


	private static final Object getObject(byte[] array, Field field, int fieldLength, String charset)
			throws UnsupportedEncodingException, InvalidBitPositionException, InvalidBodyException {

		byte[] fieldArray = Arrays.copyOfRange(array, 0, fieldLength);
		array = Arrays.copyOfRange(array, fieldLength, array.length);

		ByteBuffer buffer;
		switch (field.getDataType()) {
		case Binary:
			return fieldArray;
		case String:
			return new String(fieldArray, charset);
		case List:
			return createList(fieldArray, field.getSubfields().get(0), charset);
		case Dictionary:
			return createMap(fieldArray, field.getSubfields().get(0), field.getSubfields().get(1), charset);
		case Complex:
			return parseComplex(fieldArray, field.getSubfields(), charset);
		case Boolean:
			BitSet bitSet = BitSet.valueOf(fieldArray);
			return (bitSet.nextSetBit(0) >= 0);
		case Integer:
			buffer = ByteBuffer.allocate(Integer.BYTES);
			buffer.put(fieldArray);
			buffer.rewind();
			return buffer.getInt();
		case Long:
			buffer = ByteBuffer.allocate(Long.BYTES);
			buffer.put(fieldArray);
			buffer.rewind();
			return buffer.getLong();
		case Double:
			buffer = ByteBuffer.allocate(Double.BYTES);
			buffer.put(fieldArray);
			buffer.rewind();
			return buffer.getDouble();
		case Float:
			buffer = ByteBuffer.allocate(Float.BYTES);
			buffer.put(fieldArray);
			buffer.rewind();
			return buffer.getFloat();
		default:
			fieldLength = 0;
			return null;
		}
	}

}
