package it.ippolito.mddp.core;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Map;

import it.ippolito.mddp.contract.Contract;
import it.ippolito.mddp.contract.FunctionVersion;
import it.ippolito.mddp.domain.Header;
import it.ippolito.mddp.domain.Message;
import it.ippolito.mddp.exception.InvalidBitPositionException;
import it.ippolito.mddp.exception.InvalidBodyException;
import it.ippolito.mddp.exception.InvalidFieldLengthException;
import it.ippolito.mddp.exception.InvalidHeaderException;

public class MessageEncoder {

	
	public static byte[] encode(Message m)
			throws InvalidHeaderException, UnsupportedEncodingException, InvalidBodyException,
			InvalidBitPositionException, InvalidFieldLengthException {
		
		return encode(m.getHeader(),m.getContract(),m.getBody().getFields());

	}
	
	public static byte[] encode(Header header, Contract c, Map<String, Object> fields)
			throws InvalidHeaderException, UnsupportedEncodingException, InvalidBodyException,
			InvalidBitPositionException, InvalidFieldLengthException {

		FunctionVersion fv = c.getFunctions().get(header.getFunctionNumber()).getVersions()
				.get(header.getFunctionVersion());
		byte[] bodyByte = BodyEncoder.encode(fields, fv, header.isResponse(), c.getCharset());
		header.setMessageLength(bodyByte.length + Header.HEADER_LEN_VERSION_1);
		byte[] headerByte = HeaderEncoder.encode(header);

		ByteBuffer bb = ByteBuffer.allocate(headerByte.length + bodyByte.length);
		bb.put(headerByte);
		bb.put(bodyByte);
		return bb.array();
	}

}
