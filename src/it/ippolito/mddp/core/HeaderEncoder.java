package it.ippolito.mddp.core;

import java.nio.ByteBuffer;

import it.ippolito.mddp.domain.Header;
import it.ippolito.mddp.exception.InvalidHeaderException;
import it.ippolito.mddp.utils.MethodUtils;

public class HeaderEncoder {


	public static byte[] encode(Header header) throws InvalidHeaderException {
		if (header.getProtocolVersion() != 1) {
			throw new InvalidHeaderException();
		}
		ByteBuffer bb = ByteBuffer.allocate(Header.HEADER_LEN_VERSION_1);

		/* 4 byte - Length of the message */
		bb.put(MethodUtils.getByteFromLong(header.getMessageLength()));

		/* 1 byte - Number of the protocol Version */
		bb.put(MethodUtils.getByteFromShort(header.getProtocolVersion()));

		/*
		 * 1 byte - Field Status flag is composed by: bit 0: response message (0 for
		 * request, 1 for response) bit 1: warning obsolete Contract version bit 2:
		 * warning obsolete Function version bit 3: unknown Contract version bit 4:
		 * unknown Function version bit 5: unknown Function Number bit 6: message format
		 * error bit 7: unknown Protocol version
		 * 
		 */
		bb.put(header.getStatusFlag().toByteArray());
		/*
		 * 2 byte - Contract Version (numeric)
		 */

		bb.put(MethodUtils.getByteFromInteger(header.getContractVersion()));
		/*
		 * 
		 * 2 byte - Function Number (numeric)
		 */
		bb.put(MethodUtils.getByteFromInteger(header.getFunctionNumber()));

		/*
		 * 2 byte - Function Version (numeric)
		 */

		bb.put(MethodUtils.getByteFromInteger(header.getFunctionVersion()));

		/*
		 * 4 byte - Correlation Key (numeric)
		 */
		bb.put(MethodUtils.getByteFromLong(header.getCorrelationKey()));

		return bb.array();

	}
}
