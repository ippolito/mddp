package it.ippolito.mddp.contract;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import it.ippolito.mddp.contract.enumeration.DataStructure;
import it.ippolito.mddp.contract.enumeration.DataType;

public class Field {
	private String name;
	private String description;
	private int length;
	private DataType dataType;
	@JsonIgnoreProperties
	private DataStructure structure;
	@JsonIgnoreProperties
	private List<Field> subfields;
	
	
	/* GETTER SETTER*/
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public DataType getDataType() {
		return dataType;
	}
	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}
	public DataStructure getStructure() {
		return structure;
	}
	public void setStructure(DataStructure structure) {
		this.structure = structure;
	}
	public List<Field> getSubfields() {
		return subfields;
	}
	public void setSubfields(List<Field> subfields) {
		this.subfields = subfields;
	}
	

	@JsonIgnore
	public void print() {
		System.out.println("Name: "+ getName());
		System.out.println("Description: "+ getDescription());
		System.out.println("Length: "+ getLength());
		System.out.println("DataType: "+ getDataType());
		if(getStructure() == null ) {
			return;
		}
		System.out.println("DataStructure: "+ getStructure());
		System.out.println("Subfields");
		int i = 0;
		for(Field f : getSubfields()) {
			System.out.println("Field " + i);			
			f.print();
			i++;			
		}		
	}	

}
