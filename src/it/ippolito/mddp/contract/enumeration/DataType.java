package it.ippolito.mddp.contract.enumeration;

public enum DataType {
	Binary,
	String,
	Integer,
	Long,
	Float,
	Double,
	Boolean,
	Complex,
	List,
	Dictionary
}
