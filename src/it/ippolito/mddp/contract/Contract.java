package it.ippolito.mddp.contract;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Contract {
	
	private int version;
	private String charset;
	private List<Function> functions;
	
    public Contract() {
    }

    public Contract(int version, String charset,List<Function> versions) {
    	this.version = version;
    	this.charset = charset;
    	this.functions = versions;
    }

    
    /* Getter Setter*/
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public List<Function> getFunctions() {
		return functions;
	}

	public void setFunctions(List<Function> functions) {
		this.functions = functions;
	}
	
	@JsonIgnore
	public void print() {
		System.out.println("**************************");
		System.out.println("***** PRINTING CONTRACT ****");
		System.out.println("**************************");
		System.out.println("Version: "+ getVersion());
		System.out.println("Charset: "+ getCharset());
		int i = 0;
		for(Function f : getFunctions()) {
			System.out.println("Function "+ i);
			f.print();
			i++;			
		}
		System.out.println("**************************");
		System.out.println("***** END PRINTING *******");
		System.out.println("**************************");
	}

}
