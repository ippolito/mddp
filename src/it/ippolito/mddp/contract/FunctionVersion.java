package it.ippolito.mddp.contract;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class FunctionVersion {
	private List<Field> request;
	private List<Field> response;

	
	/* GETTER SETTER*/
	public List<Field> getRequest() {
		return request;
	}
	public void setRequest(List<Field> request) {
		this.request = request;
	}
	public List<Field> getResponse() {
		return response;
	}
	public void setResponse(List<Field> response) {
		this.response = response;
	}
	
	@JsonIgnore
	public void print() {
		System.out.println("***** FUNCTION VERSION ****");
		int i = 0;
		System.out.println("Request");
		for(Field f : getRequest()) {
			System.out.println("Field " + i);			
			f.print();
			i++;			
		}
		i = 0;
		System.out.println("Response");
		for(Field f : getResponse()) {
			System.out.println("Field " + i);			
			f.print();
			i++;			
		}
		System.out.println("***** END FUNCTION VERSION ****");

	}

}
