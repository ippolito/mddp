package it.ippolito.mddp.contract;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Function {
	private String name;
	private String description;
	private List<FunctionVersion> versions;
	
	
	/* Getter Setter*/
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<FunctionVersion> getVersions() {
		return versions;
	}
	public void setVersions(List<FunctionVersion> versions) {
		this.versions = versions;
	}
	
	
	@JsonIgnore
	public void print() {
		System.out.println("***** FUNCTION ****");
		System.out.println("Name: "+ getName());
		System.out.println("Description: "+ getDescription());
		int i = 0;
		for(FunctionVersion f : getVersions()) {
			System.out.println("FunctionVersion "+ i);
			f.print();
			i++;			
		}
		System.out.println("***** END FUNCTION ****");

	}

	

}
