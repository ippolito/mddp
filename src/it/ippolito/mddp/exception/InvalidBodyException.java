package it.ippolito.mddp.exception;

public class InvalidBodyException  extends Exception{

	private static final long serialVersionUID = 1L;
	public InvalidBodyException() {
        super();
	}

	public InvalidBodyException(String errorMessage) {
        super(errorMessage);
    }
}
