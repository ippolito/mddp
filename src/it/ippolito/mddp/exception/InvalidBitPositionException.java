package it.ippolito.mddp.exception;

public class InvalidBitPositionException  extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidBitPositionException() {
        super();
	}

	public InvalidBitPositionException(String errorMessage) {
        super(errorMessage);
    }
}
