package it.ippolito.mddp.exception;

public class BodyFormatErrorException  extends Exception{

	private static final long serialVersionUID = 1L;
	public BodyFormatErrorException() {
        super();
	}

	public BodyFormatErrorException(String errorMessage) {
        super(errorMessage);
    }
}
