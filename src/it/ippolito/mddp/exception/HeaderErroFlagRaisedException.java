package it.ippolito.mddp.exception;

public class HeaderErroFlagRaisedException  extends Exception{

	private static final long serialVersionUID = 1L;
	public HeaderErroFlagRaisedException() {
        super();
	}

	public HeaderErroFlagRaisedException(String errorMessage) {
        super(errorMessage);
    }
}
