package it.ippolito.mddp.exception;

public class InvalidContractException extends Exception {

	private static final long serialVersionUID = 1L;
	public InvalidContractException() {
        super();
	}

	public InvalidContractException(String errorMessage) {
        super(errorMessage);
    }
	public InvalidContractException(Exception e) {
        super(e);
    }
}
