package it.ippolito.mddp.exception;

public class InvalidFieldLengthException  extends Exception{

	private static final long serialVersionUID = 1L;
	public InvalidFieldLengthException() {
        super();
	}

	public InvalidFieldLengthException(String errorMessage) {
        super(errorMessage);
    }
}
