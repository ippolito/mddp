package it.ippolito.mddp.domain;

import java.util.BitSet;

import it.ippolito.mddp.contract.Contract;

/*	Class that represent the Header of the MDDP Protocol*/

public class Header {

	public static final int HEADER_LEN_VERSION_1 = 16;
	
	/* 4 byte - Length of the message	*/
	private long messageLength;
	
	/* 1 byte - Number of the protocol Version */
	private short protocolVersion;
	
	
	/* 1 byte - Field Status flag is composed by:
	 * bit 0: response message  (0 for request, 1 for response)
	 * bit 1: warning obsolete Contract version
	 * bit 2: warning obsolete Function version
	 * bit 3: unknown Contract version
	 * bit 4: unknown Function version
     * bit 5: unknown Function Number
	 * bit 6: message format error
	 * bit 7: unknown Protocol version
	 * 
	 * */
	
	public static final int BIT_0_RESPONSE_MESSAGE = 0;
	public static final int BIT_1_WARNING_OBSOLETE_CONTRACT_VERSION = 1;
	public static final int BIT_2_WARNING_OBSOLETE_FUNCTION_VERSION = 2;
	public static final int BIT_3_UNKNOWN_CONTRACT_VERSION = 3;
	public static final int BIT_4_UNKNOWN_FUNCTION_VERSION = 4;
	public static final int BIT_5_UNKNOWN_FUNCTION_NUMBER = 5;
	public static final int BIT_6_UNKNOWN_FORMAT_ERROR = 6;
	public static final int BIT_7_UNKNOWN_PROTOCOL_VERSION = 7;

	
	private BitSet statusFlag;
	/*
	 * 2 byte - Contract Version  (numeric)
	 */
	private int contractVersion;
	/*
	 * 
	 * 2 byte - Function Number	(numeric)
	*/ 
	private int functionNumber;
	
	/*
	 * 2 byte - Function Version  (numeric)
	 */
	


	private int functionVersion;

	/*
	 * 4 byte - Correlation Key  (numeric)
	 */
	
	private long correlationKey;
	
	private int headerLength;

	
	public Header() {
		
	}
	
	public Header(long messageLength,
			short protocolVersion,
			BitSet statusFlag, 
			int contractVersion,
			int functionNumber,
			int functionVersion,
			long correlationKey,
			int headerLength) {
		this. messageLength = messageLength;
		this.protocolVersion = protocolVersion;
		this.statusFlag = statusFlag;
		this.contractVersion = contractVersion;
		this.functionNumber = functionNumber;
		this.functionVersion = functionVersion;
		this.correlationKey = correlationKey;
		this.headerLength = headerLength;
	}
	

	
	public void printHeader() {
		System.out.println("**************************");
		System.out.println("***** PRINTING HEADER ****");
		System.out.println("**************************");
		System.out.println("Message Length: "+ messageLength);
		System.out.println("Protocol Version: "+ protocolVersion);
		System.out.println("Status Flag: ");
		System.out.println("	bit 0: "+statusFlag.get(0));
		System.out.println("	bit 1: "+statusFlag.get(1));
		System.out.println("	bit 2: "+statusFlag.get(2));
		System.out.println("	bit 3: "+statusFlag.get(3));
		System.out.println("	bit 4: "+statusFlag.get(4));
		System.out.println("	bit 5: "+statusFlag.get(5));
		System.out.println("	bit 6: "+statusFlag.get(6));
		System.out.println("	bit 7: "+statusFlag.get(7));
		
		
		System.out.println("Contract Version: " + contractVersion);
		System.out.println("Function Number: " + functionNumber);
		System.out.println("Function Version: " + functionVersion);
		System.out.println("Correlation Key: " + correlationKey);
		System.out.println("**************************");
		System.out.println("***** END PRINTING *******");
		System.out.println("**************************");
	}



	/* GETTER AND SETTER*/
	
	public long getMessageLength() {
		return messageLength;
	}




	public void setMessageLength(long messageLength) {
		this.messageLength = messageLength;
	}




	public short getProtocolVersion() {
		return protocolVersion;
	}




	public void setProtocolVersion(short protocolVersion) {
		this.protocolVersion = protocolVersion;
	}




	public BitSet getStatusFlag() {
		return statusFlag;
	}




	public void setStatusFlag(BitSet statusFlag) {
		this.statusFlag = statusFlag;
	}

	
	public boolean isResponse() {
		return statusFlag.get(0);
	}



	public int getContractVersion() {
		return contractVersion;
	}




	public void setContractVersion(int contractVersion) {
		this.contractVersion = contractVersion;
	}




	public int getFunctionNumber() {
		return functionNumber;
	}




	public void setFunctionNumber(int functionNumber) {
		this.functionNumber = functionNumber;
	}




	public int getFunctionVersion() {
		return functionVersion;
	}




	public void setFunctionVersion(int functionVersion) {
		this.functionVersion = functionVersion;
	}




	public long getCorrelationKey() {
		return correlationKey;
	}




	public void setCorrelationKey(long correlationKey) {
		this.correlationKey = correlationKey;
	}




	public int getHeaderLength() {
		return headerLength;
	}




	public void setHeaderLength(int headerLength) {
		this.headerLength = headerLength;
	}

	
	/* 1 byte - Field Status flag is composed by:
	 * bit 0: response message  (0 for request, 1 for response)
	 * bit 1: warning obsolete Contract version
	 * bit 2: warning obsolete Function version
	 * bit 3: unknown Contract version
	 * bit 4: unknown Function version
     * bit 5: unknown Function Number
	 * bit 6: message format error
	 * bit 7: unknown Protocol version
	 * 
	 */
	// bit 0: response message  (0 for request, 1 for response)

	public void setStatusFlagBit(boolean flag,int bit) {
			this.getStatusFlag().set(bit,flag);
	}
	
	public boolean getStatusFlag(int bit) {
		return this.getStatusFlag().get(bit);
	}

	
	public BitSet getResponseStatusFlag(Contract c) {
		BitSet bitSet = new BitSet(8);
		
		bitSet.set( BIT_0_RESPONSE_MESSAGE,true);

		
		if(c.getVersion() < this.contractVersion  ) {
			bitSet.set( BIT_3_UNKNOWN_CONTRACT_VERSION,true);
			return bitSet;
		}

		if(c.getFunctions().size() <= this.functionNumber  ) {
			bitSet.set( BIT_5_UNKNOWN_FUNCTION_NUMBER,true);
			return bitSet;
		}
		if(c.getFunctions().get(this.functionNumber).getVersions().size() <= this.functionVersion - 1  ) {
			bitSet.set( BIT_4_UNKNOWN_FUNCTION_VERSION,true);
			return bitSet; 
		}
		
		if(c.getVersion() > this.contractVersion  ) {
			bitSet.set( BIT_1_WARNING_OBSOLETE_CONTRACT_VERSION,true);
		}
		
		if(c.getFunctions().get(this.functionNumber).getVersions().size() > this.functionVersion - 1  ) {
			bitSet.set( BIT_2_WARNING_OBSOLETE_FUNCTION_VERSION,true);
		}		
		 return bitSet;
	}
	
}
