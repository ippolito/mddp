package it.ippolito.mddp.domain;

import java.util.Map;

public class Body {

	private Map<String, Object> fields;
	private String charset;

	public Body() {
	}

	public Body(Map<String, Object> fields, String charset) {
		this.fields = fields;
		this.charset = charset;
	}


	/* GETTER AND SETTER */

	public Map<String, Object> getFields() {
		return fields;
	}

	public void setFields(Map<String, Object> fields) {
		this.fields = fields;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}
}
