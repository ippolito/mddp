package it.ippolito.mddp.domain;

import it.ippolito.mddp.contract.Contract;

public class Message {

	
	private Header header;
	private Body body;
	private Contract contract;
		
	public Message(Header header, Body body, Contract contract) {
		this.header = header;
		this.body = body;
		this.contract = contract;
	}

	
	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}
	

}
