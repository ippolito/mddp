# MDDP
Application Protocol for server-2-server communication. For more infor read the [MDDP Protocol documentation](http://paoloippolito.altervista.org/pdf/MDDP_EN_v1.0.pdf) 


## License
Copyright 2022 Paolo Ippolito <ippolito.paolo.rec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
